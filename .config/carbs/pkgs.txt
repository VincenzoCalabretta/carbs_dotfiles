# |||| systems packages ||||
wireguard-tools
perl-file-mimeinfo
fd
ripgrep-all
zsh
networkmanager
openresolv
# |||| window manager ||||
polkit
i3
#sway
#wl-copy
#check if wl-copy is adequate
#xorg-xwayland
i3status
#foot
#kanshi
brightnessctl
playerctl
# |||| basic applications ||||
gnome-keyring
neovim
nvim-packer-git
python-six     #per vim-mediawiki-editor
python-pynvim  #per vim-mediawiki-editor
python-mwclient    #per vim-mediawiki-editor
man-db
julia
unzip
lf
htop
nextcloud-client
keepassxc
tree
wmenu
wget
neomutt
# |||| extra applications ||||
neofetch
ncmpcpp
zathura
zathura-pdf-mupdf
pandoc
chromium
# |||| appended applications||||
tmux
wget
ttf-inconsolata
#clipman
fzf
tldr
zsh-syntax-highlighting-git
#jupyter
#quarto-cli-bin
dmenu
alacritty
bluez
bluez-utils
imagemagick
anki
xournal
weylux
weylus
scrot
xclip
scrot
libselinux
