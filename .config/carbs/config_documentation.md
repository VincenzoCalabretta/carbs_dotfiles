#Hi! this is the carbs documentation


#### tmux
What are the feautures that you found useful?:

- sessioni
- processi multipli nel medesimo terminale
- permette di far girare in background processi staccandoli dal terminale

la feauture piu' interessante sembra essere: sessioni

di default le sessioni sono perse al riavvio
e' disponibile un 
prefisso di default di tmux e ctrl+b

come posso attaccharmi faciemente ad una sessione?
come e con quale finalita' posso integrare tmux nel workflow?

sarebbe utile avere un comando comen ctrl+a nel terminale che permette di entrare
in tmux(quando parlo di entrare a cosa mi riferisco? tmux attach forse?)




#### vim-mediawiki
modifiche fatte per ignorare certificato ssl

g:mediawiki_editor_url
g:mediawiki_editor_path
g:mediawiki_editor_username
g:mediawiki_editor_password
g:mediawiki_editor_uri_scheme


```
    version = mwclient.__dict__.get(
        "__version__"
    ) or mwclient.__dict__.get("__ver__")
    version_tuple = [int(n) for n in version.split(".")]

    if version_tuple >= [0, 10, 0]:
        s = mwclient.Site(
            base_url,
            httpauth=basic_auth_creds,
            path=mediawiki_path,
            scheme=uri_scheme,
            reqs={'verify': False}
        )
    else:
        s = mwclient.Site(
            (uri_scheme, base_url),
            httpauth=basic_auth_creds,
        )

    s.login(username, password, domain=domain)

    return s
```
