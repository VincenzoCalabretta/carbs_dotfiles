#!/bin/bash
#

# check if the touchpad is toggled or not

# Check if the file exists and is readable
TF="$XDG_CONFIG_HOME/carbs/touchpad_is_toggled"
echo "setting"
if [ -r $TF ]; then
    # Search for the number 2 in the file
    if grep -q "1" "$TF"; then
	    echo "the touchpad is toggled, disable it"
	    uwtpctl off
	    echo 0 > $TF
    elif grep -q "0" "$TF"; then
	    echo "the touchpad is not toggled enable it"
	    uwtpctl on
	    echo 1 > $TF
    else
        echo "File does not contain 0 or 1"
    fi
else
    echo "File does not exist or is not readable"
fi




