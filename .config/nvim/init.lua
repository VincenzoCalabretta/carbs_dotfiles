--[[
This is how you have multiline comments
Thank you TJ
--]]

-- Set <space> as the leader key
-- See `:help mapleader`
--  NOTE: Must happen before plugins are required (otherwise wrong leader will be used)
vim.g.mapleader = ' '
vim.g.maplocalleader = ' '

-- [[ Package manager ]]
require 'config.01_packages'
-- [[ Setting options ]]
require 'config.02_settings'
-- [[ Basic Keymaps ]]
require 'config.03_keymaps'
-- [[ Configure Telescope ]]
require 'config.04_telescope_config'
-- [[ Configure Treesitter ]]
require 'config.05_treesitter_config'
-- [[ Configure LSP ]]
require 'config.06_lsp_config'
-- [[ Configure nvim-cmp ]]
require 'config.07_cmp_config'
-- [[ Configure languagetool ]]
require 'config.08_ltex'


print(vim.g.slime_target)
-- The line beneath this is called `modeline`. See `:help modeline`
-- utile per esempio per settare indentazione specifica per un tipo di file
-- vim: ts=2 sts=2 sw=2 et
